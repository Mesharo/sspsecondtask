# Snake game - singleplayer, multiplayer

## Zadání

Budu implementovat hru Snake v prostředí pygame. Hra bude pro max. 3  hráče, z nichž každý ovládá jednoho hada na záčátku délky 1 políčko pomocí 4 kláves.
        V hracím poli se bude náhodně objevovat jídlo a jed. Pokud had sní jed, had umírá. Pokud had sní jídlo, naroste o jedno políčko. Pokud had narazí do stěny,               umírá. Pokud had narazí sám do svého těla, nebo do těla protihráčova hada, umírá.
        Hra končí, když umřou všichni hadi v poli.
        Vyhrává ten hráč, který bude mít po ukončení hry nejdelšího hada.

Samotný program najdete v záložce snakegame.py

### K čemu projekt slouží

Hlavně pro zábavu, řešení grafiky pomocí snížení FPS dodává hře pravý *old school* styl, a hráč může se špetkou nostalgie zavzpomínat na své herní začátky.

### Použití

Po spuštění se Vás program zeptá, kolik hráčů má v plánu tuto hru hrát. Na Vás je napsat číslo od 1 do 3 a potvrdit klávesou enter.
Po zvolení se Vám otevře nové okno a jste uvedeni přímo do hry.
Pokud hrajete pouze sami, pak má Váš had červenou barvu a ovládá se šipkami.
Pokud hrajete ve dvou, pak hráč 1 má hada červené barvy s ovládacími klávesami šipkami, druhý hráč má hada zelené barvy s ovládacími klávesami WSAD.
Pokud hrajete ve třech, pak třetí hráč má hada modré barvy s ovládacími klávesami IKJL.

Hra končí, pokud umřou všichni hráči v poli. Hru můžete kdykoliv ukončit křížkem v pravém horním rohu.

Aby se had otočil a změnil tím směr, stačí nám ovládací klávesu zmáčknout pouze jednou a had si tento nově zvolený směr udržuje, pokud budete opakovaně mačkat tlačítko i přes to, že to očividně funguje, nebude poté ovládání přesné!

### Instalace

Zkopírujete obsah repozitáře do lokální složky, poté v terminálu pomocí

```
cd <absolutníCestaKeSložce>
```

najedete do vaší složky, a příkazem

```
python snakegame.py
```

spustíte hru.

### Prerekvizity

1. Python - nejnovější verze ještě nepodporuje pygame! Vystačíte si s verzí 3.10.8 </br> [Python](https://www.python.org/downloads/)
1. pygame [Pygame](https://www.pygame.org/wiki/GettingStarted)

Přeji hodně štěstí při hře!
